<?php

/**
 * yvBBCode - BBCode Plugin, developed for Joomla! 1.6
 * @version		$Id: yvbbcode.php 7 2013-02-09 12:38:58Z yvolk $
 * @package		yvBBCodePlugin
 * @copyright	2007-2011 yvolk (Yuri Volkov), http://yurivolkov.com. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * @modified		by Bart Jochems (info@batjo.nl) 2011 to run on Joomla! 1.6
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

define('yvBBCodeVersion', '2.1.1');
define('JPATH_SITE_YVBBCODE', dirname(__FILE__));
define('JPATH_SITE_YVBBCODE_ASSETS', dirname(__FILE__));

require_once (JPATH_SITE_YVBBCODE_ASSETS . DS . 'BBCodeParser.php');
require_once (JPATH_SITE_YVBBCODE_ASSETS . DS . 'BBCodeParser' . DS .  'Filter.php');

class plgSystemyvbbcode extends JPlugin {
	var $_initialized = false;
	var $_instanceId = 0;

	var $_debug = false;
	// Remove BBCode from title tag
	var $_removeFromTitle = true;

	var $_bbcodeset_path = '';
	// Groups of BBCodes (as defined in BBCodeParser...)
	var $_filters = array ();
	var $_abbcode = array ();
	//Array with sorted _abbcode for replacements
	var $_abbcodeHeader = array ();
	var $_parser = null;

	/**
	 * Constructor
	 */
	function plgSystemYvbbcode(& $subject, $config) {
		parent :: __construct($subject, $config);
		$this->_instanceId = rand(1000, 9999);
		//echo 'yvBBCode ' . $this->_instanceId . ' created.<br/>';
	}

	// The error handler is not used yet... maybe some time later :-)
	//	function _yvBBCode_errorHandler($errcode, $errstring, $filename=null, $lineno=null, $context=null) {
	//		$mainframe = JFactory::getApplication();
	//		$mainframe->enqueueMessage('errcode=' .$errcode . '; errstring="' . $errstring , '"', 'error');
	//	}

	function onAfterRoute() {
		$this->_initialize();
	}

	// Show clickable BBCodes
	function onBBCode_RenderForm($id_of_target_control, & $Form) {
		return $this->_renderForm($id_of_target_control, $Form);
	}

	// Replace codes of BBCodes with HTML tags
	function onBBCode_RenderText(& $string_to_process) {
		$results = $this->_replaceCode($string_to_process);
	}


	// We don't use "onContentPrepare" here, 
	// because "onContentPrepare" is not called from "blog" views in Joomla! 1.6
	function onContentAfterTitle($context, & $article, & $params, $page = 0) {
		if ($this->params->get('articletext', 0)) {
			$debug = false;
			if (isset($article->catid)) {
				if ($debug) {
					echo 'context=', $context, '; ';
					var_dump($article);
				}
				if ($this->_articleCategoryEnabled($article->catid)) {
					if (isset($article->text)) {
						$this->_replaceCode($article->text);
						if ($debug) {
							echo 'CodeReplaced text="', $article->text, '"; ';
						}
					} else {
						if (isset($article->introtext)) {
							$this->_replaceCode($article->introtext);
							if ($debug) {
								echo 'CodeReplaced intro="', $article->introtext, '"; ';
							}
						}
						if (isset($article->fulltext)) {
							$this->_replaceCode($article->fulltext);
							if ($debug) {
								echo 'CodeReplaced full="', $article->fulltext, '"; ';
							}
						}
					}
				}
			} else {
				if ($debug) {
					echo 'No catid, context=', $context, '; ';
					//$this->_replaceCode($article->text);
				}
			}
		}
		return '';
	}

	// Do BBCode replacements on the whole page
	function onAfterRender() {
		if ($this->params->get('pagetext', 0)) {
			$document = JFactory :: getDocument();
			$doctype = $document->getType();
			if ($doctype == 'html') {
				$body = JResponse :: getBody();
				if ($this->_replaceCode($body)) {
					//$body .= 'yvBBCode - ' . $doctype . '<br/>';
					JResponse :: setBody($body);
				}
			}
		}
	}

	// returns boolean Ok
	function _initialize() {
		$mainframe = JFactory::getApplication();
		if ($this->_initialized) {
			JError::raiseWarning( '1' , 'yvBBCode instanceId=' . $this->_instanceId . ' was initialized already');
			return true;
		}
		//echo 'yvBBCode ' . $this->_instanceId . ' initializes...<br/>';

		$this->_initialized = true;
		$document = JFactory :: getDocument();
		$doctype = $document->getType();

		// Only render for HTML output
		if ($doctype !== 'html') {
			return false;
		}

		$Ok = true;
		$message = '';
		$path = '';
		$url = '';
		$bbcodeset_baseurl = '';

		$lang = JFactory :: getLanguage();
		$lang->load('plg_system_yvbbcode', JPATH_ADMINISTRATOR);

		$this->_debug = (boolean) $this->_getConfigValue('debug', '0');
		$this->_removeFromTitle = (boolean) $this->_getConfigValue('remove_from_title', '1');

		$path = $this->_getConfigValue('bbcodeset_path', 'editor/default.xml');
		//echo 'bbcodeset_path="' . $path . '"<br />';
		$path = str_replace('/', DS, $path);
		$url = str_replace('\\', '/', $path);

		if (substr($path, 0, 6) == 'editor') {
			// path, relative from this file
			$pos = strrpos($url, '/');
			if ($pos > 0) {
				$bbcodeset_baseurl = 'plugins/system/yvbbcode/' . substr($url, 0, $pos +1);
			}
			$path = JPATH_SITE_YVBBCODE . DS . $path;
		} else
		if (substr($url, 0, 1) == '/') {
			// starts with '/' - this is path, relative from site root
			$url = substr($url, 1);
			$pos = strrpos($url, '/');
			if ($pos > 0) {
				$bbcodeset_baseurl = substr($url, 0, $pos +1);
			}
			$path = JPATH_SITE . $path;
		}

		if (!file_exists($path)) {
			$Ok = false;
			$message .= 'The BBCode set file doesn\'t exist="' . $path . '"<br />';
		} else {
			$this->_bbcodeset_path = $path;
			$Ok = $this->_readBBCodeSet($message, $bbcodeset_baseurl);
		}

		$options = array();
		$options['filters'] = $this->_filters;

		// initialize BBCode parser
		$this->_parser = new HTML_BBCodeParser($options);

		if ($this->_getConfigValue('usedefaultcss', '1')) {
			$document->addStyleSheet($this->_getSiteURL() . 'plugins/system/yvbbcode/default.css', 'text/css');
		}
		$document->addScript($this->_getSiteURL() . 'plugins/system/yvbbcode/default.js', 'text/javascript');
		if (!$Ok) {
			$this->_debug = false;
		}
		if ($this->_debug) {
			//$mainframe = JFactory::getApplication();
			$mainframe->enqueueMessage($this->_TestMe(), 'notice');
		}

		if (strlen($message) > 0) {
			$message .= 'BBCode set file="' . $this->_bbcodeset_path . '"';
			$message .= $this->_textSignature();
			$mainframe->enqueueMessage($message, ($Ok ? 'notice' : 'error'));
		}
		return ($Ok);
	}

	// Test loaded BBCodes
	// Returns HTML string - result of test
	function _TestMe() {
		$ControlID = 'document.forms.yvBBCodeDebugForm' . $this->_instanceId . '.fulltext';

		$strOut = '<h3>yvBBCode test results</h3>';
		$strOut .= '1. Try to insert BBCodes in a Form:<br />';
		$this->_renderForm($ControlID, $strOut);
		$strOut .= '<form id=\'yvBBCodeDebugForm' . $this->_instanceId . '\' >';
		$strOut .= '<textarea id=\'fulltext\' rows=\'3\' cols=\'80\' >something</textarea><br />';

		$strOut .= '<hr>2. Test replacements of BBCodes:<br />';
		$nRows = 0;
		$src = '';
		$result = '';
		foreach ($this->_abbcode as $row) {
			$nRows += 1;
			$tag = $row[0];
			$test = $row[3];
			$src .= $tag . ': ' . $test . chr(10);
			if ($this->_replaceCode($test)) {
				$result .= $tag . ': ' . $test . '<br />';
			}
		}
		if ($nRows>0) {
			$strOut .= '<textarea id=\'src1\' rows=\'' . $nRows . '\' cols=\'80\' >' . $src . '</textarea><br />';
		}
		$strOut .= '</form>';
		$strOut .= $result;
		$strOut .= '<input size=45 value="[b]BBCode inside input is not changed[/b]"/><br />';
		$strOut .= 'BBCode set file=\'' . $this->_bbcodeset_path . '\'<br />';
		$strOut .= $this->_textSignature();

		return $strOut;
	}

	function _readBBCodeSet(& $message, $bbcodeset_baseurl = '') {
		$mainframe = JFactory::getApplication();
		$Ok = true;

		jimport('joomla.utilities.simplexml');

		$xml = new JSimpleXML();
		$xml->loadFile($this->_bbcodeset_path);
		if (!$xml->document) {
			$message .= 'Error loading BBCodeSet file<br />';
			$Ok = false;
		}

		// Create list of filters
		// These filters should be also defined in /BBCodeParser/Filter folder
		if ($Ok) {
			$filters = null;
			// (PHP 5 >= 5.1.0)
			//if (property_exists($xml->document, 'bbcodes')) {
			if (isset($xml->document->filters)) {
				$filters = $xml->document->filters[0];
			}
			if (!$filters) {
				$message .= '"filters" element not found in BBCodeSet file ?? Maybe this is wrong XML file?<br />';
				$Ok = false;
			}
		}
		if ($Ok) {
			foreach ($filters->children() as $child) {
				//print_r($child);
				$filter = trim($child->data());
				if ($this->_getConfigValue('filter_' . $filter, '0')) {
					$this->_filters[] = $filter;
				}
			} // foreach
			//$message .= 'Filters: ' . print_r($this->_filters, true) . '<br />';
		}

		if ($Ok) {
			$bbcodes = null;
			// (PHP 5 >= 5.1.0)
			//if (property_exists($xml->document, 'bbcodes')) {
			if (isset($xml->document->bbcodes)) {
				$bbcodes = $xml->document->bbcodes[0];
			}
			if (!$bbcodes) {
				$message .= '"bbcodes" element not found in BBCodeSet file ?? Maybe this is wrong XML file?<br />';
				$Ok = false;
			}
		}
		if ($Ok) {
			$ind1 = 0;
			foreach ($bbcodes->children() as $child) {
				if ($child->name() == 'bbcode') {
					//print_r($child);
					$filter = trim($child->filter[0]->data());
					if (in_array($filter, $this->_filters)) {
						$alt = trim($child->name[0]->data());
						$alt = JText :: _($alt);
						$src = trim($child->file[0]->data());
						$tag = trim($child->tag[0]->data());
						if ($this->_debug) {
							$test = trim($child->test[0]->data());
						}
						if (strlen($src) > 0) {
							$posColon = strpos($src, ':');
							if (!($posColon > 0)) {
								// relative path
								if ((substr($src, 0, 1) == '/')) {
									$src = $this->_getSiteURL() . substr($src, 1);
								} else {
									// relative from $bbcodeset_path
									$src = $this->_getSiteURL() . $bbcodeset_baseurl . $src;
								}
							}
							//echo '<img src="' . $src . '" alt="' . $alt . '">';
							$skip = false;
							if (!$skip) {
								$this->_abbcode[$ind1][0] = $tag;
								$this->_abbcode[$ind1][1] = $alt;
								$this->_abbcode[$ind1][2] = $src;
								if ($this->_debug) {
									$this->_abbcode[$ind1][3] = $test;
								}
							}
							if (!$skip) {
								$ind1++;
							}
						}
					}
				}
			} // foreach
		}

		return $Ok;
	}

	// Show clickable BBCodes
	// $ControlID - id of target control
	// $Form - string, to which the Form code is appended
	function _renderForm($ControlID, & $Form) {
		$strOut = '';
		$strOut .= '<div class=\'yvBBCodeForm\'>';
		$src_prev = '';
		foreach ($this->_abbcode as $row) {
			$tag = $row[0];
			$tag = str_replace('\'', '\\\'', $tag);
			$alt = $row[1];
			$src = $row[2];
			;
			if (strcmp($src_prev, $src) != 0) {
				$strOut .= '<a href="javascript:void(0);" onclick="yvBBCodeReplaceText(' . $ControlID . ', \'' . $tag . '\');">';
				$strOut .= '<img src="' . $src . '" alt="' . $alt . '" />';
				$strOut .= '</a>';
				$src_prev = $src;
			}
		}
		$strOut .= '</div>';
		$Form .= $strOut;
		return true;
	}

	/* Replace codes of BBCodes with tags
	 * $text - String to process  	*/
	function _replaceCode(& $text) {
		$debug = false;
		$message = '';

		if ($this->_parser == null) {
			if ($debug) {
				$message .= 'BBCode parser is null<br />';
				if (!empty($message)) {
					$text .= $message;
				}
			}
			return;
		}

		// Don't replace BBCodes or something else in square brackes :-)
		//   inside these elements (tags):
		$tagsToExclude = array('title', 'textarea', 'input', 'script', 'select');

		$text = '*' . $text; // for strpos
		$strOut = '';
		$start1 = 0; // start offset
		$end1 = strlen($text); // end offset

		while ($start1 < $end1) {
			if ($debug) {
				$message .= ' start1=' . $start1 . ' end1=' . $end1 . '; ';
			}
			//find nearest tag to exclude
			$tag3 = '';
			$start3 = $end1;
			$end3 = $end1;
			foreach( $tagsToExclude as $tagToExclude) {
				$start4 = strpos( $text, '<' . $tagToExclude , $start1);
				if	(($start4 > 0) && ($start4 < $start3)) {
					$tag3 = $tagToExclude;
					$start3 = $start4;
				}
			}
			if ($start3 < $end1) {
				// tag to exclude was found
				if ($debug) {
					$message .= 'tag=' . $tag3 . ' start=' . ($start3 + strlen($tag3)) . '; ';
				}
				//find end of element
				// 1. End of this tag
				$end4 = strpos( $text, '>', ($start3 + strlen($tag3)));
				if	(($end4 > 0) && ($end4 < $end3) && (substr($text, $end4 - 1, 1) == '/')) {
					if ($debug) {
						$message .= 'EndOfThisTag=' . $end4;
					}
					$end3 = $end4;
				} else {
					// 2. Or closing element
					$end4 = strpos( $text, '</' . $tag3 , ($start3 + strlen($tag3)));
					if ($debug) {
						$message .= 'ClosingTag=' . $end4;
					}
					if	(($end4 > 0) && ($end4 < $end3)) {
						$end4 = strpos($text, '>', $end4 +1);
					}
					if ($debug) {
						$message .= ' end4=' . $end4;
					}
					if	(($end4 > 0) && ($end4 < $end3)) {
						$end3 = $end4 + 1;
					}
				}
			}
			if ($debug) {
				$message .= ' start3=' . $start3 . ' end3=' . $end3;
			}
				
			if ($start3 > $start1) {
				// replace before tag to exclude
				$str1 = substr($text, $start1, $start3 - $start1);
				$this->_parser->setText($str1);
				$this->_parser->parse();
				$str2 = $this->_parser->getParsed();
				$strOut .= $str2;
			}
			if ($start3 < $end1) {
				// don't replace here
				$str1 = substr($text, $start3, $end3 - $start3);
				if ($this->_removeFromTitle) {
					if ($tag3 == 'title') {
						$this->_removeCode($str1);
					}
				}
				$strOut .= $str1;
			}
				
			$start1 = $end3;
			if ($debug) {
				$message .= '<br />';
			}
		};

		// remove first character, that was added;
		$text = substr($strOut, 1);

		if ($debug) {
			if (!empty($message)) {
				$text .= $message;
			}
		}
		return true;
	}

	/* Remove BBCodes
	 * $text - String to process  	*/
	function _removeCode(& $text) {
		$debug = false;
		$message = 'removing code ';

		$text = '*' . $text; // for strpos
		$strOut = '';
		$start1 = 0; // start offset
		$end1 = strlen($text); // end offset

		while ($start1 < $end1) {
			if ($debug) {
				$message .= ' start1=' . $start1 . ' end1=' . $end1 . '; ';
			}
			//find nearest BBCode
			$start3 = $end1;
			$end3 = $end1;

			$start4 = strpos( $text, '[' , $start1);
			if	(($start4 > 0) && ($start4 < $start3)) {
				$start3 = $start4;
			}
			if ($start3 < $end1) {
				//find end of BBCode
				$end4 = strpos( $text, ']', $start3);
				if	($end4 < 1) {
					// next tag...
					$end4 = strpos( $text, '<', $start3) - 1;
				}
				if	(($end4 > 0) && ($end4 < $end3)) {
					$end3 = $end4 + 1;
				}
			}
			if ($debug) {
				$message .= ' start3=' . $start3 . ' end3=' . $end3;
			}
				
			if ($start3 > $start1) {
				// copy from start
				$str1 = substr($text, $start1, $start3 - $start1);
				$strOut .= $str1;
			}
			$start1 = $end3;
			if ($debug) {
				$message .= '<br />';
			}
		};

		// remove first character, that was added;
		$text = substr($strOut, 1);

		if ($debug) {
			if (!empty($message)) {
				$text .= $message;
			}
		}
		return true;
	}

	// Is Category with this ID enabled for some operation...
	function _articleCategoryEnabled($ArticleCategoryID) {
		static $sEnabled = null;
		static $aCategoryID = array ();
		static $blnExclude = false;
		if ($sEnabled == null) {
			$articlecategoryids = trim($this->_getConfigValue('articlecategoryids', ''));
			$sEnabled = (strlen($articlecategoryids) == 0);
			if (!$sEnabled) {
				$aCategoryID = explode(",", $articlecategoryids);
				$blnExclude = (boolean) $this->_getConfigValue('articlecategoryids_exclude', '0');
			}
		}
		$Enabled = $sEnabled;
		if (!$sEnabled) {
			$blnFound = false;
			foreach ($aCategoryID as $categoryid1) {
				if ((int) $categoryid1 == $ArticleCategoryID) {
					$blnFound = true;
					break;
				}
			}

			if ($blnFound) {
				$Enabled = !$blnExclude;
			} else {
				$Enabled = $blnExclude;
			}
		}
		return $Enabled;
	}

	// returns value of the Extension parameter
	function _getConfigValue($paramName = '', $default = '') {
		$value = $default;

		switch ($paramName) {
			case 'access' :
				if ($this->params) {
					$value = 0;
				} else {
					// if Plugin is not loaded, then Access is denied
					$value = 0; //999;
				}
				break;
			default :
				if ($this->params) {
					$value = $this->params->get($paramName, $default);
				}
		}
		//echo '_getConfigValue param="' . $paramName . '", value="' . $value . '"<br/>';

		return $value;
	}

	// Signature of this Extension
	function _textSignature() {
		$message = '<br/>-- <br/>' .
		'<a href="http://yurivolkov.com/Joomla/yvBBCode/index_en.html" target="_blank">' .
		'yvBBCode extension</a>, version="' .
		yvBBCodeVersion . '"';
		return $message;
	}

	function _getSiteURL() {
		$mainframe = JFactory::getApplication();
		$uri = JFactory::getURI();
		return ($mainframe->isAdmin() ? $uri->root() : JURI :: base());
	}


}
?>
