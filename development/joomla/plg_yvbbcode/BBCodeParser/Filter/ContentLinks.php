<?php
/**
 * Joomla! 1.5 Content links filter
 * based on BBCodeParser Links.php by Stijn de Reede  <sjr@gmx.co.uk>
 *
 * yvBBCode - BBCode Plugin, developed for Joomla! 1.5
 *
 * @version		$Id: ContentLinks.php 7 2013-02-09 12:38:58Z yvolk $
 * @package		yvBBCodePlugin
 * @author	  yvolk (Yuri Volkov) <http://yurivolkov.com>
 * @copyright	2007-2011 yvolk (Yuri Volkov), http://yurivolkov.com. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 */

// Required for function ContentIDToURL
require_once (JPATH_SITE.DS.'includes'.DS.'application.php' );
require_once (JPATH_SITE.DS.'components'.DS.'com_content'.DS.'helpers'.DS.'route.php');

/**
 *
 */
class HTML_BBCodeParser_Filter_ContentLinks extends HTML_BBCodeParser_Filter
{

	/**
	 * An array of tags parsed by the engine
	 *
	 * @access   private
	 * @var      array
	 */
	var $_definedTags = array(
        'contentid' => array(
            'htmlopen'  => 'a',
            'htmlclose' => 'a',
            'allowed'   => 'none^img',
            'attributes'=> array('contentid' => 'href=%2$s%1$s%2$s')
	),
	// the same, but open in new window
        'contentidn' => array(
            'htmlopen'  => 'a target=\'_blank\'',
            'htmlclose' => 'a',
            'allowed'   => 'none^img',
            'attributes'=> array('contentidn' => 'href=%2$s%1$s%2$s')
	)
	);

	private $found = false;

	/**
	 * Executes statements before the actual array building starts
	 *
	 * This method should be overwritten in a filter if you want to do
	 * something before the parsing process starts. This can be useful to
	 * allow certain short alternative tags which then can be converted into
	 * proper tags with preg_replace() calls.
	 * The main class walks through all the filters and and calls this
	 * method if it exists. The filters should modify their private $_text
	 * variable.
	 *
	 * @return   none
	 * @access   private
	 * @see      $_text
	 * @author   Stijn de Reede <sjr@gmx.co.uk>
	 */
	function _preparse()
	{
		$options = HTML_BBCodeParser::getStaticProperty('HTML_BBCodeParser', '_options');
		$o = $options['open'];
		$c = $options['close'];
		$oe = $options['open_esc'];
		$ce = $options['close_esc'];

		//$this->_preparsed = $this->_text;
		//$pattern1 = array("!".$oe."contentid(".$ce."|\s.*".$ce.")(.*)".$oe."/contentid".$ce."!Ui");
		//$replace1 = array( $o."contentid=\\2\\1\\2".$o."/contentid".$c);
		//$pp = preg_replace($pattern1, $replace1, $this->_text);
		$pp = $this->_text;

		$this->found = false;
		//$pattern2 = "!".$oe."contentid=([0-9]+)([^\s\[\]]*)".$ce."!i";
		//$pattern2 = "!".$oe."contentid=([0-9]+)([^\s\[\]]*)".$ce."([^\s\[\]]*)".$oe."/contentid".$ce."!i";
		$pattern2 = "!".$oe."contentid=([^\[\]]*)".$ce."([^\[\]]*)".$oe."/contentid".$ce."!i";
		$this->_preparsed = preg_replace_callback($pattern2, array($this, 'replaceContentid'), $pp);

		// For some reason this doesn't work,
		// so one can not use [contentidn] BBCode directly...
		//if (!$this->found) {
		//	$pattern2 = "!".$oe."contentidn=([^\[\]]*)".$ce."([^\[\]]*)".$oe."/contentidn".$ce."!i";
		//	$this->_preparsed = preg_replace_callback($pattern2, array($this, 'replaceContentid'), $pp);
		//}
		//$this->_preparsed = $pp;
	}

	function replaceContentid($matches) {
		$mainframe = JFactory::getApplication();

		$options = HTML_BBCodeParser::getStaticProperty('HTML_BBCodeParser','_options');
		$o = $options['open'];
		$c = $options['close'];
		$debug = false;

		$id = (int) $matches[1];
		$text = $matches[2];

		$this->found = true;

		$urlContent = self::ContentIDToURL($id, false);

		$ret = '';
		if ($debug) {
			$ret .= '<br>start';
			$ret .= '; Route="' .  $urlContent . '" ';
			$ret .= '; matchers="' . preg_replace('([\[\]]+)', '*', print_r($matches, true)) . '" ';
			$ret .= '<br>replaced with: ';
		}
		if ($urlContent == '') {
			$ret .= $text;
		} else {
			if ($mainframe->isAdmin()) {
				$ret .= $o .'contentidn="'.$urlContent. '"' . $c . $text .$o.'/contentidn'.$c;
			} else {
				//$ret .= $o .'contentid='.$urlContent. $c . $text .$o.'/contentid'.$c;
				//$ret .= $o .'url='.$urlContent. $c . $text .$o.'/url'.$c;
				$ret .= $o .'contentid="'.$urlContent. '"' . $c . $text .$o.'/contentid'.$c;
			}
		}
		if ($debug) {
			$ret .= '<br>end<br>';
		}
		return $ret;
	}

	/* Build URL to the Article by it's ID, taking SEF settings,
	 * security for UserID etc. into account
	 * @version		ContentIDToURL 0010 2011-02-22 00:00:00Z yvolk $
	 * @author	  yvolk (Yuri Volkov) <http://yurivolkov.com>
	 * Based on code from getList function of 'modules/mod_latestnews/helper.php'
	 * It is very strange, that Joomla didn't have such function yet...
	 * It has now :-), but the implementation is far from ideal...
	 * 
	 * v.009 Optionally $fragment may be added to the URL
	 * v.010 Modified for Joomla! 1.6
	 */
	static function ContentIDToURL($id_in, $pathonly = true, $UserID = null, $ShowUnpublished = false, $fragment = "") {
		$debug = false;
		//$debug = ($id_in == 21);
		$message = '';
		$id = (integer) $id_in;
		$url = '';
		if ($id > 0) {
			$mainframe = JFactory::getApplication();

			$db		= JFactory::getDBO();
			$user	= JFactory::getUser($UserID);
			$groups = $user->getAuthorisedViewLevels();

			$contentConfig = JComponentHelper::getParams( 'com_content' );
			$access		= !$contentConfig->get('shownoauth');
			$authorized = !$access;
			$where = '';
			if ($debug) {
				$message .= 'articleid=' . $id_in . '<br/>';
			}

			// Just like it is done in 'components/com_content/models/article.php'
			$query = $db->getQuery(true);

			// If badcats is not null, this means that the article is inside an unpublished category
			// In this case, the state is set to 0 to indicate Unpublished (even if the article state is Published)
			$query->select('a.id, a.alias, ' .
				'CASE WHEN badcats.id is null THEN a.state ELSE 0 END AS state, ' .
				'a.mask, a.catid, ' .
				'a.publish_up, a.publish_down, ' .
				'a.parentid, ' .
				'a.access' );
			$query->from('#__content AS a');

			// Join on category table.
			$query->select('c.alias AS category_alias, c.access AS category_access');
			$query->join('LEFT', '#__categories AS c on c.id = a.catid');

			// Join over the categories to get parent category titles
			$query->select('parent.id as parent_id, parent.alias as parent_alias');
			$query->join('LEFT', '#__categories as parent ON parent.id = c.parent_id');

			$query->where('a.id = ' . (int) $id);

			if (!$ShowUnpublished) {
				// Links to the archived content are enabled, so state = -1
				$query->where('a.state IN(-1, 1)');

				// Filter by start and end dates.
				$nullDate = $db->Quote($db->getNullDate());
				$nowDate = $db->Quote(JFactory::getDate()->toMySQL());

				$query->where('(a.publish_up = ' . $nullDate . ' OR a.publish_up <= ' . $nowDate . ')');
				$query->where('(a.publish_down = ' . $nullDate . ' OR a.publish_down >= ' . $nowDate . ')');

				// Join to check for category published state in parent categories up the tree
				// If all categories are published, badcats.id will be null, and we just use the article state
				$subquery = ' (SELECT cat.id as id FROM #__categories AS cat JOIN #__categories AS parent ';
				$subquery .= 'ON cat.lft BETWEEN parent.lft AND parent.rgt ';
				$subquery .= 'WHERE parent.extension = ' . $db->quote('com_content');
				$subquery .= ' AND parent.published <= 0 GROUP BY cat.id)';
				$query->join('LEFT OUTER', $subquery . ' AS badcats ON badcats.id = c.id');
			}

			$db->setQuery($query);

			$item = $db->loadObject();
			if (!is_null($item)) {

				if (!$authorized) {
					if ($item->catid == 0 || $item->category_access === null) {
						$authorized = in_array($item->access, $groups);
					}
					else {
						$authorized = in_array($item->access, $groups) && in_array($item->category_access, $groups);
					}
				}
				if ($authorized) {
					if ($mainframe->isAdmin()) {
						// TODO: 2011-02-11 yvolk I couldn't find any example of how to create a link
						// to the article preview from backend, only to the 'edit' task :-(
						// like in '/administrator/components/com_content/views/featured/tmpl/default.php':
						$route = 'index.php?option=com_content&task=article.edit&return=featured&id='.$item->id;						 
						if (strlen($fragment) > 0) {
							$route .= '#' . $fragment;
						}
						$url = JRoute::_($route);						 
						if ($debug) {
							$message .= 'isAdmin, url_1=\'' . $url . '\'<br/>';
						}
					} else {
						// Build URL
						// Just like it is done in 'components/com_content/views/article/view.html.php'

						// Add router helpers.
						$item->slug			= $item->alias ? ($item->id.':'.$item->alias) : $item->id;
						$item->catslug		= $item->category_alias ? ($item->catid.':'.$item->category_alias) : $item->catid;
						$item->parent_slug	= $item->category_alias ? ($item->parent_id.':'.$item->parent_alias) : $item->parent_id;

						// TODO: Change based on shownoauth
						$route = ContentHelperRoute::getArticleRoute($item->slug, $item->catslug);						 
						if (strlen($fragment) > 0) {
							$route .= '#' . $fragment;
						}
						$url = JRoute::_($route);						 
						if ($debug) {
							$message .= 'slug=' . $item->slug . '<br/>';
							$message .= 'url_1=\'' . $url . '\'<br/>';
						}

						// This hack is needed for the backend.
						// Oherwise the link would lead to the Article manager...
						if ( (!$pathonly)
						|| (substr($url, 0, 1) != '/')
						) {
							// Code from JURI::root()
							$uri = JURI::getInstance(JURI::base());
							$root = array();
							$root['prefix'] = $uri->toString( array('scheme', 'host', 'port') );
							$root['path']   = rtrim($uri->toString( array('path') ), '/\\');

							if (substr($url, 0, 1) != '/') {
								// Maybe this will not be executed...
								// Do we need 'path' of the 'base' here?
								// $url = $root['path'] . '/' . $url;
								$url = '/' . $url;
							}
							if (!$pathonly) {
								$url = $root['prefix'] . $url;
							}
							if ($debug) {
								$message .= 'prefix=\'' . $root['prefix'] . '\'<br/>';
								$message .= 'path=\'' . $root['path'] . '\'<br/>';
							}
						}
					}
					if (strlen($fragment) > 0) {
						// 2009-07-11 There is Joomla! bug in v.1.5.12:
						// JURI class remembers the 'fragment' of the processed URI and uses it
						// in subsequent calls to build URIs...
						// So let's clear this fragment
						$uri =& JFactory::getURI();
						$uri->setFragment('');
					}
				} else {
					if ($debug) {
						$message .= 'access denied<br/>';
					}
				}
				if ($access && $debug) {
					$message .= '$item->access="' . $item->access . '";';
					if ($item->catid != 0 && $item->category_access !== null) {
						$message .= '$item->category_access="' . $item->category_access . '";';
					}
					$message .=	'groups="' . print_r($groups, true) . '";'
					. '<br/>';
				}
			}	else {
				if ($debug) {
					$message .= 'row is null<br/>';
					if (function_exists('printDbErrors')) {
						$message .= self::printDbErrors($db);
					} else {
						$message .= ';DB error: "' . $db->getErrorMsg(false) . '"';
					}
				}
			}
		}
		if ($debug) {
			$message .= 'url_3=\'' . $url . '\'<br/>';
		}
		if ($message) {
			//$mainframe = JFactory::getApplication();
			//$mainframe->enqueueMessage($message, 'notice');
			$url .= '?DEBUGMSGSTART' . $message . 'DEBUGMSGEND';
		}
		return $url;
	}

}
?>