<?php
/*
* yvBBCode - A BBCode system extension (Plugin) for Joomla! 1.5
* @version		$Id: COPYRIGHT.php 7 2013-02-09 12:38:58Z yvolk $
* @package		yvBBCode
* @copyright	2007-2013 yvolk (Yuri Volkov), http://yurivolkov.com. All rights reserved.
* @license	GPL, see LICENSE.php
*/
?>

yvBBCode is copyrighted work of Yuri Volkov.
Copyright (c) 2007-2013 yvolk (Yuri Volkov), http://yurivolkov.com. All rights reserved.

yvBBCode includes or is derivative of works distributed under the following copyright notices:

Language packs
---
Copyright:	Translators of Language packs (to be expanded).
License:	GNU General Public License (GPL)

yvSmiley	- used as template for this extension
---
WEB:		http://yurivolkov.com/Joomla/yvSmiley/index_en.html
Copyright:	2007-2011 yvolk (Yuri Volkov), http://yurivolkov.com. All rights reserved.
License:	GNU General Public License (GPL)

HTML_BBCodeParser (part of PEAR Package) v.1.2.2 - parser library
---
WEB:		http://pear.php.net/package/HTML_BBCodeParser
Copyright:	1997-2003 The PHP Group
License:	version 2.02 of the PHP license

Advanced BBCode Box MOD for phpBB - BBCode images
---
WEB:		http://www.tactic.be
MOD Author: Disturbed One < anthony@anthonycoy.com > (Anthony Coy) http://www.hvmdesign.com
MOD Author: AL Tnen < al_tnen@hotmail.com > (N/A) http://www.tnen.zzn.com
MOD Author: freddie < freddie@tactic.be > (Stefaan Van Damme) http://www.tactic.be
Copyright:	2007 phpBB Group, All Rights Reserved.
License:	GNU General Public License v2

Joomla!
---
WEB:		http://www.joomla.org/
Copyright:	2005 - 2007 Open Source Matters. All rights reserved.
License:	GNU General Public License (GPL)
