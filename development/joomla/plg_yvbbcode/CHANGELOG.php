<?php
/** 
* yvBBCode - BBCode Plugin, developed for Joomla! 1.5
* This file contains changelog of this extension since v.1.02.000
* 
* @version		$Id: CHANGELOG.php 7 2013-02-09 12:38:58Z yvolk $
* @package		yvBBCodePlugin
* @copyright	2007-2013 yvolk (Yuri Volkov), http://yurivolkov.com. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
Legend:

* -> Security Fix
# -> Bug Fix
$ -> Language fix or change
+ -> Addition
^ -> Change
- -> Removed
! -> Note

v.2.02.000 09-February-2013 yvolk
 ^ Updated for Joomla! v.2.5. No "strict standards" warnings any more.

v.2.01.001 06-May-2011 yvolk
 # Fixed bug causing unnecessary '1' symbol shown on a page, see http://forum.joomla.org/viewtopic.php?f=579&t=617697

v.2.01.000 14-March-2011 yvolk
 ^ Improved BBCode replacement: BBCodes inside HTML tags are not replaces with HTML tags (as earlier, that caused markup error) but removed.
 ^ Content links [contentid] code was updated 

v.2.00.001 11-February-2011 yvolk
 # [contentid] BBCode fixed - link to the content item
 # 'global $mainframe' references fixed

v.2.00.000 08-February-2011 yvolk
 ^ Modified by Bart Jochems (info@batjo.nl) and yvolk to run on Joomla! 1.6
 ^ Parameters that mention 'SectionIDs of Articles' were renamed to 'CategoryIDs...' because there are no 'Sections' in Joomla 1.6
 ! [contentid] and [contentidn] - links to the content items don't work yet.

v.1.03.001 10-February-2009 yvolk
 # Content link bug, if Joomla was installed not in the root of the site (function ContentIDToURL v.008)

v.1.03.000 23-December-2008 yvolk
 + Added 'Remove BBCodes from page title' option (default to 'Yes')

v.1.02.000 25-October-2008 yvolk
 + Added [br] BBCode (to the 'Extended BBCodes') to have line breaks anywhere
 # Updated function, that generates link to the Article for	[contentid] (function ContentIDToURL) to v.007
 $ All translations were moved to yvBBCodeLanguagePacks
 
