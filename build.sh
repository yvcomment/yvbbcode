#!/bin/bash
PROJECTNAME="yvBBCode"
# ----------------------------------------------------------------------------
# $Id: $
# Execute default build action using "phing",
# see http://phing.info/
# ----------------------------------------------------------------------------
function pause {
	echo ""
	read -p "Please press Enter to continue..." nothing
}

echo "-------------------------"
echo "Build $PROJECTNAME"

MYROOT=$(dirname $0)
echo "Root Dir: $MYROOT"
cd $MYROOT
echo -n "Current dir: "
pwd

phing

echo -n "-------------------------"
pause
exit 0