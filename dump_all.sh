#!/bin/bash
PROJECTNAME="yvBBCode"
# ----------------------------------------------------------------------------
# $Id: dump_all.sh 14 2009-10-25 15:44:20Z yvolk $
# Dump whole $PROJECTNAME solution related artefacts using "phing",
# see http://phing.info/
# ----------------------------------------------------------------------------
# Manually increase this variable after each release... 
MAJORVERSION="2.01.000"
# The directory of the dump files is located outside of the Repository
dumpDir="../Archives"

function pause {
	echo ""
	read -p "Please press Enter to continue..." nothing
}

echo "-------------------------"
echo "Dumping all $PROJECTNAME solution related artifacts"

MYROOT=$(dirname $0)
cd $MYROOT
echo -n "Root Dir:       " 
pwd

MYFILE=""
VERSION=""
for (( intv=100; intv>0;  intv-- )); do
	printf -v VERSION1 "%03d" $intv
	MYFILE1="$dumpDir/$PROJECTNAME-$MAJORVERSION.$VERSION1-dump.zip"
#	echo "Looking for $MYFILE1"
 	if [ -e $MYFILE1 ]; then
 		break
#		echo "The file already exists: $PROJECTNAME-$MAJORVERSION.$VERSION-dump.zip"
	else
		MYFILE=$MYFILE1
		VERSION=$VERSION1
 	fi
done

if [ "$VERSION" != "" ]; then
	blnYes=""
	echo "The file will be created:   $MYFILE"
	read -p "Create the file? (type y for yes):	" blnYes
	case $blnYes in 
		"y" | "Y") echo " Yes" ;;
		*) echo " No"; 
			VERSION="" ;;
	esac
fi

if [ "$VERSION" = "" ]; then
	while true; do
		read -p "Version number $MAJORVERSION." intv
		printf -v VERSION1 "%03d" $intv
		if [ "$VERSION1" = "000" ]; then
			break
		fi
		MYFILE1="$dumpDir/$PROJECTNAME-$MAJORVERSION.$VERSION1-dump.zip"
	 	if [ ! -e $MYFILE1 ]; then
			MYFILE=$MYFILE1
			VERSION=$VERSION1
	 		break
	 	else
			echo "The file already exists: $PROJECTNAME-$MAJORVERSION.$VERSION1-dump.zip"
	 	fi
	done
fi

if [ "$VERSION" = "" ]; then
	echo "Nothing to do"
else
	phing dump_all -Dver=$MAJORVERSION.$VERSION
fi

cd $MYROOT
echo -n "Current dir: "
pwd
echo -n "-------------------------"
pause
exit 0
 
